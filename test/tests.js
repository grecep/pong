QUnit.test("getPixelValue", function (assert) {
    assert.equal(getPixelValue(12), '12px');
    assert.equal(getPixelValue(-12), '-12px');
});

QUnit.test("getWidthAndHeightInRatio", function (assert) {
    assert.deepEqual(getWidthAndHeightInRatio(100, 100, 0.5), {width: 100, height: 50});
    assert.deepEqual(getWidthAndHeightInRatio(100, 100, 2), {width: 50, height: 100});
});

QUnit.test("Score test", function (assert) {
    var div1 = document.getElementById('testDiv1');
    var div2 = document.getElementById('testDiv2');
    var score = new Score(div1, div2, {});
    score.playerScored(true);
    score.playerScored(true);
    score.playerScored(false);
    assert.equal(div1.innerText, "2");
    assert.equal(div2.innerText, "1");
    score.playerScored(false);
    score.playerScored(false);
    assert.equal(div1.innerText, "2");
    assert.equal(div2.innerText, "3");
});

QUnit.test("Paddle test", function (assert) {
    var paddleDiv = document.getElementById('testDiv1');
    var containerDiv = document.getElementById('testDiv2');
    var paddleContainerWidth = 100;
    var gameFieldWidth = 1000;
    var centerY = 250;
    var paddleHeight = 100;
    var dimensions = {
        paddleContainerWidth: paddleContainerWidth, gameFieldWidth: gameFieldWidth,
        centerY: centerY, paddleHeight: paddleHeight
    };
    var paddleLeft = new Paddle(paddleDiv, containerDiv, dimensions, true);
    checkPaddle(assert, paddleLeft, containerDiv, 100, 200, "200px");

    var paddleRight = new Paddle(paddleDiv, containerDiv, dimensions, false);
    checkPaddle(assert, paddleRight, containerDiv, 900, 200, "200px");
});

function checkPaddle(assert, paddle, containerDiv, expectedBounceLine, expectedTopY, expectedContainerTop) {
    assert.equal(paddle.getBounceLineX(), expectedBounceLine);
    assert.equal(paddle.getTopY(), expectedTopY);
    paddle.render();
    assert.equal(containerDiv.style.top, expectedContainerTop);
}