function getPixelValue(value) {
    return value + 'px';
}

function getWidthAndHeightInRatio(width, height, ratio) {
    if (height / width > ratio) {
        height = width * ratio;
    } else {
        width = height / ratio;
    }
    return {width: width, height: height};
}