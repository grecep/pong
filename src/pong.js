window.onload = pong;

// For now reload game when orientation changes, can improve later to continue the same match
window.addEventListener("orientationchange", function () {
    location.reload();
});

function pong() {

    var paddleLeft = null;
    var paddleRight = null;
    var ball = null;
    var previousTimestamp = -1;

    var gameConfiguration = {
        gameFieldRatio: 1 / 2,
        initialSpeed: 1 / 5, // ratio of game field width per second
        speedIncreaseFactor: 1.2, // multiplier of speed when ball touches a paddle
        missedPaddleAnimationLength: 1000,
        missedPaddleAnimationOneColorLength: 200,
        maximalBallYDirection: 1.5 // ball shouldn't move more than that in vertical direction, when it moves for one unit in horizontal direction
    };

    function Dimensions(gameFieldWidth, gameFieldHeight) {
        this.gameFieldWidth = gameFieldWidth;
        this.gameFieldHeight = gameFieldHeight;
        this.centerY = gameFieldHeight / 2;
        this.ballSize = gameFieldHeight / 20;
        this.paddleHeight = gameFieldHeight / 6;
        this.paddleWidth = this.paddleHeight / 4;
        this.paddleContainerWidth = this.paddleWidth * 4;
        this.scoreHeight = gameFieldHeight / 6;
        this.scoreWidth = gameFieldWidth * 0.3;
        this.scorePosition = gameFieldWidth * 0.1;
        this.initialSpeed = gameFieldWidth * gameConfiguration.initialSpeed / 1000;
    }

    (function startGame() {
        var gameDiv = document.getElementById('gameField');
        var dimensions = getGameDimensionsWithAspectRatio(gameDiv);
        setMiddleLine(document.getElementById('middleLine'), dimensions);
        var ballDiv = document.getElementById('ball');
        var leftPaddleDiv = document.getElementById('leftPaddle');
        var rightPaddleDiv = document.getElementById('rightPaddle');
        var leftPaddleContainer = document.getElementById('leftTouchContainer');
        var rightPaddleContainer = document.getElementById('rightTouchContainer');
        var leftScoreDiv = document.getElementById('scoreLeft');
        var rightScoreDiv = document.getElementById('scoreRight');
        var score = new Score(leftScoreDiv, rightScoreDiv, dimensions);
        paddleLeft = new Paddle(leftPaddleDiv, leftPaddleContainer, dimensions, true);
        paddleRight = new Paddle(rightPaddleDiv, rightPaddleContainer, dimensions, false);
        ball = new Ball(ballDiv, paddleLeft, paddleRight, score, dimensions, gameConfiguration);
        preventSwipeToRefreshOnSides(dimensions);
        gameLoop()
    })();

    function gameLoop(timestamp) {
        var timeDiff = 0;
        if (previousTimestamp >= 0 && timestamp - previousTimestamp < 200) {
            timeDiff = timestamp - previousTimestamp;
        }
        previousTimestamp = timestamp;
        updateState(timeDiff);
        render();
        requestAnimationFrame(gameLoop); // baje obstaja nek polyfill za tam kjer ne dela
    }

    function updateState(timeDiff) {
        ball.updateBall(timeDiff);
    }

    function render() {
        ball.render();
        paddleLeft.render();
        paddleRight.render();
    }

    function setMiddleLine(middleLine, dimensions) {
        middleLine.style.height = getPixelValue(dimensions.gameFieldHeight);
        middleLine.style.left = getPixelValue(dimensions.gameFieldWidth / 2 - middleLine.clientWidth / 2);
    }

    function getGameDimensionsWithAspectRatio(gameDiv) {
        var rationedFieldSize = getWidthAndHeightInRatio(gameDiv.clientWidth, gameDiv.clientHeight, gameConfiguration.gameFieldRatio);
        return new Dimensions(rationedFieldSize.width, rationedFieldSize.height);
    }

    // prevent swipe to refresh on sides of the screen,
    // so that players wouldn't restart the game by accident
    function preventSwipeToRefreshOnSides(dimensions) {
        document.addEventListener('touchstart', function (evt) {
            var touches = evt.targetTouches;
            if (touches.length > 0) {
                var touchX = touches[0].pageX;
                if (touchX < dimensions.gameFieldWidth * 0.25 || touchX > dimensions.gameFieldWidth * 0.75) {
                    evt.preventDefault();
                }
            }
        }, {passive: false});
    }
}

(function() {
    // https://gist.github.com/paulirish/1579671
    // http://paulirish.com/2011/requestanimationframe-for-smart-animating/
    // http://my.opera.com/emoller/blog/2011/12/20/requestanimationframe-for-smart-er-animating
    // requestAnimationFrame polyfill by Erik Möller. fixes from Paul Irish and Tino Zijdel
    // MIT license
    var lastTime = 0;
    var vendors = ['ms', 'moz', 'webkit', 'o'];
    for(var x = 0; x < vendors.length && !window.requestAnimationFrame; ++x) {
        window.requestAnimationFrame = window[vendors[x]+'RequestAnimationFrame'];
        window.cancelAnimationFrame = window[vendors[x]+'CancelAnimationFrame']
            || window[vendors[x]+'CancelRequestAnimationFrame'];
    }

    if (!window.requestAnimationFrame)
        window.requestAnimationFrame = function(callback, element) {
            var currTime = new Date().getTime();
            var timeToCall = Math.max(0, 33 - (currTime - lastTime));
            var id = window.setTimeout(function() { callback(currTime + timeToCall); },
                timeToCall);
            lastTime = currTime + timeToCall;
            return id;
        };

    if (!window.cancelAnimationFrame)
        window.cancelAnimationFrame = function(id) {
            clearTimeout(id);
        };
}());