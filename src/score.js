function Score(leftScoreDiv, rightScoreDiv, dimensions) {
    var scoresForPlayer = {right: 0, left: 0};

    (function initViews() {
        leftScoreDiv.style.left = getPixelValue(dimensions.scorePosition);
        rightScoreDiv.style.left = getPixelValue(dimensions.gameFieldWidth - dimensions.scoreWidth - dimensions.scorePosition);
        leftScoreDiv.style.width = getPixelValue(dimensions.scoreWidth);
        rightScoreDiv.style.width = getPixelValue(dimensions.scoreWidth);
        leftScoreDiv.style.fontSize = getPixelValue(dimensions.scoreHeight);
        rightScoreDiv.style.fontSize = getPixelValue(dimensions.scoreHeight);
    })();

    this.playerScored = function (isLeftPlayer) {
        if (isLeftPlayer) {
            increaseScore("left", leftScoreDiv);
        } else {
            increaseScore("right", rightScoreDiv);
        }
    };

    function increaseScore(playerString, scoreDiv) {
        scoresForPlayer[playerString]++;
        renderScore(scoreDiv, scoresForPlayer[playerString]);
    }

    function renderScore(scoreDiv, value) {
        scoreDiv.innerText = value;
    }
}