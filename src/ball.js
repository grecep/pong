function Ball(ballDiv, paddleLeft, paddleRight, score, dimensions, gameConfiguration) {
    var ballDirection = {};
    var ballPosition = {};
    var ballSpeed = 1;
    var missedPaddleAnimationRemainingTime = -1;
    var missedPaddleAnimation = false;

    var previousPosition = {x: 0, y: 0};
    var movement = {x: 0, y: 0};

    this.updateBall = function updateBall(timeDiff) {
        if (missedPaddleAnimation) {
            missedPaddleAnimate(timeDiff);
            return;
        }
        previousPosition.x = ballPosition.x;
        previousPosition.y = ballPosition.y;
        movement.x = ballDirection.x * ballSpeed * timeDiff;
        movement.y = ballDirection.y * ballSpeed * timeDiff;
        ballPosition.x += movement.x;
        ballPosition.y += movement.y;

        // check if crossing vertical line of paddle
        var leftPaddleLine = paddleLeft.getBounceLineX();
        var crossingLeftLine = checkPaddleCrossing(paddleLeft, leftPaddleLine, previousPosition, ballPosition, movement, true);
        if (!crossingLeftLine) {
            var rightPaddleLine = paddleRight.getBounceLineX() - dimensions.ballSize;
            checkPaddleCrossing(paddleRight, rightPaddleLine, previousPosition, ballPosition, movement, false);
        }
        checkIfReachedTopBottom();
    };

    this.render = function render() {
        ballDiv.style.left = getPixelValue(ballPosition.x);
        ballDiv.style.top = getPixelValue(ballPosition.y);
    };

    (function initBall() {
        ballDiv.style.width = getPixelValue(dimensions.ballSize);
        ballDiv.style.height = getPixelValue(dimensions.ballSize);
        resetBall();
    })();

    function checkPaddleCrossing(paddle, paddleLine, previousPosition, ballPosition, movement, isLeftSide) {
        var isBallCrossingPaddleLine = isLeftSide ? (previousPosition.x > paddleLine && ballPosition.x <= paddleLine) :
            (previousPosition.x < paddleLine && ballPosition.x >= paddleLine);
        if (isBallCrossingPaddleLine) {
            var ratioToPaddle = (paddleLine - previousPosition.x) / movement.x;
            var ballYAtPaddleLine = previousPosition.y + movement.y * ratioToPaddle;
            var paddleTopY = paddle.getTopY();
            if (ballYAtPaddleLine + dimensions.ballSize > paddleTopY && ballYAtPaddleLine < paddleTopY + dimensions.paddleHeight) {
                var percentagePositionOnPaddle = (ballYAtPaddleLine + dimensions.ballSize - paddleTopY) / (dimensions.paddleHeight + dimensions.ballSize);
                ballTouchedPaddle(percentagePositionOnPaddle);
            } else {
                ballMissedPaddle(isLeftSide);
            }
            ballPosition.x = paddleLine;
            ballPosition.y = ballYAtPaddleLine;
            return true;
        }
        return false;
    }

    function resetBall() {
        var directionX = Math.random() < 0.5 ? -1 : 1;
        ballSpeed = dimensions.initialSpeed;
        ballDirection = {x: directionX, y: 1};
        ballPosition = {x: dimensions.gameFieldWidth / 2, y: dimensions.gameFieldHeight / 2}
        paddleLeft.setMovementEnabled(true);
        paddleRight.setMovementEnabled(true);
    }

    function missedPaddleAnimate(timeDiff) {
        missedPaddleAnimationRemainingTime -= timeDiff;
        if (missedPaddleAnimationRemainingTime <= 0) {
            missedPaddleAnimation = false;
            ballDiv.style.backgroundColor = 'white';
            resetBall()
        } else {
            if (Math.floor(missedPaddleAnimationRemainingTime / gameConfiguration.missedPaddleAnimationOneColorLength) % 2 === 0) {
                ballDiv.style.backgroundColor = 'red';
            } else {
                ballDiv.style.backgroundColor = 'white';
            }
        }
    }

    function ballMissedPaddle(leftSide) {
        ballSpeed = 0;
        missedPaddleAnimation = true;
        missedPaddleAnimationRemainingTime = gameConfiguration.missedPaddleAnimationLength;
        score.playerScored(!leftSide);
        paddleLeft.setMovementEnabled(false);
        paddleRight.setMovementEnabled(false);
    }

    function ballTouchedPaddle(percentagePositionOnPaddle) {
        calculateDirectionAfterPaddleTouch(percentagePositionOnPaddle);
        var maxSpeed = 1;
        ballSpeed *= gameConfiguration.speedIncreaseFactor;
        ballSpeed = Math.min(ballSpeed, maxSpeed);
    }

    function calculateDirectionAfterPaddleTouch(percentagePositionOnPaddle) {
        ballDirection.x = -ballDirection.x;
        var yDirectionChange = (percentagePositionOnPaddle - 0.5) + (Math.random() - 0.5)/2;
        var newYDirection = ballDirection.y + yDirectionChange;
        newYDirection = Math.min(newYDirection, gameConfiguration.maximalBallYDirection);
        newYDirection = Math.max(newYDirection, -gameConfiguration.maximalBallYDirection);
        ballDirection.y = newYDirection;
    }

    function checkIfReachedTopBottom() {
        // handle ball reaching top/bottom of screen
        if (ballPosition.y + dimensions.ballSize >= dimensions.gameFieldHeight) {
            ballTouchedTopBottom();
            ballPosition.y = dimensions.gameFieldHeight - dimensions.ballSize;
        } else if (ballPosition.y <= 0) {
            ballTouchedTopBottom();
            ballPosition.y = 0;
        }
    }

    function ballTouchedTopBottom() {
        ballDirection.y = -ballDirection.y;
    }
}