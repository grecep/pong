function Paddle(paddleDiv, paddleTouchContainer, dimensions, isLeft) {
    var movementEnabled = false;
    var paddleTopY = 0;
    var touchStartedTopOffset = 0;

    this.setMovementEnabled = function (enabled) {
        movementEnabled = enabled;
    };

    this.getBounceLineX = function () {
        if (isLeft) {
            return dimensions.paddleContainerWidth;
        } else {
            return dimensions.gameFieldWidth - dimensions.paddleContainerWidth;
        }
    };
    this.getTopY = function () {
        return paddleTopY;
    };

    this.render = function render() {
        paddleTouchContainer.style.top = getPixelValue(paddleTopY);
    };

    (function initView() {
        if (isLeft) {
            paddleTouchContainer.style.left = getPixelValue(0);
        } else {
            paddleTouchContainer.style.left = getPixelValue(dimensions.gameFieldWidth - dimensions.paddleContainerWidth);
        }
        paddleTouchContainer.style.height = getPixelValue(dimensions.paddleHeight);
        paddleDiv.style.width = getPixelValue(dimensions.paddleWidth);
        paddleTouchContainer.style.width = getPixelValue(dimensions.paddleContainerWidth);
        paddleTopY = dimensions.centerY - dimensions.paddleHeight / 2;
        setTouchListeners(paddleTouchContainer);
    })();

    function setTouchListeners(touchArea) {
        touchArea.addEventListener("touchstart", handleStart);
        touchArea.addEventListener("touchmove", handleMove);

        function handleStart(evt) {
            evt.preventDefault();
            if (!movementEnabled) return;

            var touches = evt.targetTouches;
            if (touches.length > 0) {
                var touchY = touches[0].pageY;
                touchStartedTopOffset = touchY - paddleTopY;
            }
        }

        function handleMove(evt) {
            evt.preventDefault();
            if (!movementEnabled) return;

            var touches = evt.targetTouches;
            var touch = touches[touches.length - 1];
            paddleTopY = touch.pageY - touchStartedTopOffset;

            // check if moved out of bounds
            if (paddleTopY + dimensions.paddleHeight > dimensions.gameFieldHeight) {
                paddleTopY = dimensions.gameFieldHeight - dimensions.paddleHeight;
            } else if (paddleTopY < 0) {
                paddleTopY = 0;
            }
        }
    }

}